import {createSlice} from '@reduxjs/toolkit'
import { loginThunk } from './thunk'
import { User } from 'types'

type QuanLyNguoiDungInitialState = {
    user?: User
    accessToken?: string
}

const initialState: QuanLyNguoiDungInitialState = {
    accessToken: localStorage.getItem('accessToken')
}

const quanLyNguoiDungSlice = createSlice({
    name: 'quanLyNguoiDung',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            // .addCase(loginThunk.pending, (state, { payload }) => {
            //     console.log('payload pending: ', payload);
            // })
            .addCase(loginThunk.fulfilled, (state, { payload })=>{
                console.log('payload fulfilled: ', payload);
                state.user = payload

                // lưu thông tin đăng nhập vào localstorage
                if(payload){
                    localStorage.setItem('accessToken', payload.accessToken)
                }
            })
            // .addCase(loginThunk.rejected, (state, { payload })=>{
            //     console.log('payload rejected: ', payload);
            // })
    },
})

export const {reducer: quanLyNguoiDungReducer, actions: quanLyNguoiDungActions} = quanLyNguoiDungSlice