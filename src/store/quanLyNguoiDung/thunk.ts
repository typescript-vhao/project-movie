import { createAsyncThunk } from "@reduxjs/toolkit";
import { LoginSchemaType } from "schema";
import { quanLyNguoiDungServices } from "services";

export const loginThunk = createAsyncThunk(
    'quanLyNguoiDung/loginThunk', 
    async (payload: LoginSchemaType, {rejectWithValue}) => {
        try {
            const data = await quanLyNguoiDungServices.login(payload)
            return data.data.content
        } catch (err) {
            return rejectWithValue(err)
        }
})

export const getUser = createAsyncThunk(
    'quanLyNguoiDung/getUser'
)