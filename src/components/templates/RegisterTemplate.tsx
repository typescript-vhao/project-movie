import { zodResolver } from "@hookform/resolvers/zod";
import { SubmitHandler, useForm } from "react-hook-form";
import { RegisterSchema, RegisterSchemaType } from "schema";
import { quanLyNguoiDungServices } from "services";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
import { PATH } from "constant";

export const RegisterTemplate = () => {
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<RegisterSchemaType>({
    mode: "onChange",
    resolver: zodResolver(RegisterSchema),
  });
  const navigate = useNavigate();
  const onSubmit: SubmitHandler<RegisterSchemaType> = async (value) => {
    try {
      console.log({ value });
      await quanLyNguoiDungServices.register(value);
      toast.success("Đăng ký thành công!");
      navigate(PATH.login);
    } catch (err) {
      console.log(err);
      toast.error(err?.response?.data?.content);
    }
  };
  return (
    <form className="my-6" onSubmit={handleSubmit(onSubmit)}>
      <div>
        <h2 className="text-white text-3xl font-semibold text-center">
          Register
        </h2>
        <div className="mt-6">
          <input
            type="text"
            placeholder="Tài khoản"
            className="outline-none block w-full p-2 text-white border border-white rounded-lg bg-[#333]"
            // {...register("taiKhoan", {
            //   required: "Vui lòng nhập tài khoản",
            //   minLength: {
            //     value: 6,
            //     message: "Nhập từ 6 ký tự",
            //   },
            //   maxLength: {
            //     value: 20,
            //     message: "Nhập tối đa 20 ký tự",
            //   },
            // })}
            {...register("taiKhoan")}
          />
          <p className="text-red-500">{errors?.taiKhoan?.message}</p>
        </div>
        <div className="mt-5">
          <input
            type="password"
            placeholder="Mật khẩu"
            className="outline-none block w-full p-2 text-white border border-white rounded-lg bg-[#333]"
            {...register("matKhau")}
          />
          <p className="text-red-500">{errors?.matKhau?.message}</p>
        </div>
        <div className="mt-5">
          <input
            type="text"
            placeholder="Email"
            className="outline-none block w-full p-2 text-white border border-white rounded-lg bg-[#333]"
            {...register("email")}
          />
          <p className="text-red-500">{errors?.email?.message}</p>
        </div>
        <div className="mt-5">
          <input
            type="text"
            placeholder="Số ĐT"
            className="outline-none block w-full p-2 text-white border border-white rounded-lg bg-[#333]"
            {...register("soDt")}
          />
          <p className="text-red-500">{errors?.soDt?.message}</p>
        </div>
        <div className="mt-5">
          <input
            type="text"
            placeholder="Mã nhóm"
            className="outline-none block w-full p-2 text-white border border-white rounded-lg bg-[#333]"
            {...register("maNhom")}
          />
          <p className="text-red-500">{errors?.maNhom?.message}</p>
        </div>
        <div className="mt-5">
          <input
            type="text"
            placeholder="Họ tên"
            className="outline-none block w-full p-2 text-white border border-white rounded-lg bg-[#333]"
            {...register("hoTen")}
          />
          <p className="text-red-500">{errors?.hoTen?.message}</p>
        </div>
      </div>
      <div className="mt-6">
        <button className="text-white bg-red-500 font-normal rounded-lg text-sm px-5 py-2.5 w-full">
          Register
        </button>
      </div>
    </form>
  );
};
