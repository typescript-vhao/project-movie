import { zodResolver } from "@hookform/resolvers/zod";
import { useNavigate } from "react-router-dom";
import { PATH } from "constant";
import { SubmitHandler, useForm } from "react-hook-form";
import { LoginSchema, LoginSchemaType } from "schema";
// import { quanLyNguoiDungServices } from "services";
import { toast } from "react-toastify";
import { useAppDispatch } from "store";
import { loginThunk } from "store/quanLyNguoiDung/thunk";
import { Input } from "components/ui";

export const LoginTemplate = () => {
  const navigate = useNavigate();
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<LoginSchemaType>({
    mode: "onChange",
    resolver: zodResolver(LoginSchema),
  });
  const dispatch = useAppDispatch();
  const onSubmit: SubmitHandler<LoginSchemaType> = async (value) => {
    // try {
    //   await quanLyNguoiDungServices.login(value);
    //   toast.success("Đăng nhập thành công!");
    //   navigate("/");
    // } catch (err) {
    //   toast.error(err?.response?.data?.content);
    // }
    dispatch(loginThunk(value))
      .unwrap()
      .then(() => {
        toast.success("Đăng nhập thành công!");
        navigate("/");
      });
  };
  return (
    <form className="my-6" onSubmit={handleSubmit(onSubmit)}>
      <div>
        <h2 className="text-white text-3xl font-semibold text-center">
          Sign In
        </h2>
        <div className="mt-7">
          {/* <input
            type="text"
            placeholder="Tài khoản"
            className="outline-none block w-full p-2 text-white border border-white rounded-lg bg-[#333]"
            {...register("taiKhoan")}
          />
          <p className="text-red-500">{errors?.taiKhoan?.message}</p> */}
          <Input
            register={register}
            name="taiKhoan"
            error={errors?.taiKhoan?.message}
            placeholder="Tài khoản"
            type="text"
          />
        </div>
        <div className="mt-5">
          {/* <input
            type="password"
            placeholder="Mật khẩu"
            className="outline-none block w-full p-2 text-white border border-white rounded-lg bg-[#333]"
            {...register("matKhau")}
          />
          <p className="text-red-500">{errors?.matKhau?.message}</p> */}
          <Input
            register={register}
            name="matKhau"
            error={errors?.matKhau?.message}
            placeholder="Mật khẩu"
            type="password"
          />
        </div>
      </div>
      <div className="mt-6">
        <button className="text-white bg-red-500 font-normal rounded-lg text-sm px-5 py-2.5 w-full">
          Sign In
        </button>
        <p className="text-white mt-3">
          Chưa có tài khoản?{" "}
          <span
            className="text-blue-500 cursor-pointer"
            onClick={() => {
              navigate(PATH.register);
            }}
          >
            Đăng ký
          </span>
        </p>
      </div>
    </form>
  );
};
