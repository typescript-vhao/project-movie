import { NavLink, useNavigate } from "react-router-dom";
import styled from "styled-components";
import { Avatar, Popover } from ".";
import { UserOutlined } from "@ant-design/icons";
import { PATH } from "constant";

export const Header = () => {
  const navigate = useNavigate();
  return (
    <HeaderS>
      <div className="header-content">
        <h2 className="font-medium text-[23px]">CYBER MOVIE</h2>
        <div className="flex justify-around items-center">
          <NavLink to={PATH.about} className="mr-5">
            About
          </NavLink>
          <NavLink to={PATH.contact}>Contact</NavLink>
          <Popover
            content={
              <div className="p-2">
                <h2 className="font-medium mb-2 p-1">Nguyễn Vũ Hào</h2>
                <hr />
                <div
                  className="p-1 mt-1 cursor-pointer hover:bg-gray-500 hover:text-white rounded-lg transition-all duration-300"
                  onClick={() => {
                    navigate(PATH.account);
                  }}
                >
                  Thông tin tài khoản
                </div>
                <div className="p-1 mt-1 cursor-pointer hover:bg-gray-500 hover:text-white rounded-lg transition-all duration-300">
                  Đăng xuất
                </div>
              </div>
            }
            trigger="click"
          >
            <Avatar
              className="!ml-4 cursor-pointer"
              size={27}
              icon={<UserOutlined />}
            />
          </Popover>
        </div>
      </div>
    </HeaderS>
  );
};

const HeaderS = styled.header`
  height: var(--header-height);
  background: white;
  box-shadow: 0 0 5px rgba(1, 1, 1, 0.4);
  .header-content {
    max-width: var(--max-width);
    margin: auto;
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 100%;
  }
`;
