import { HTMLInputTypeAttribute } from "react";
import { UseFormRegister } from "react-hook-form";

type InputProps = {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  register?: UseFormRegister<any>;
  error?: string;
  name?: string;
  placeholder?: string;
  type?: HTMLInputTypeAttribute;
};

export const Input = ({
  register,
  error,
  name,
  placeholder,
  type,
}: InputProps) => {
  return (
    <div>
      <input
        type={type}
        placeholder={placeholder}
        className="outline-none block w-full p-2 text-white border border-white rounded-lg bg-[#333]"
        {...register(name)}
      />
      {error && <p className="text-red-500">{error}</p>}
    </div>
  );
};
