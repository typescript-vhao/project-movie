import { HomeTemplate } from "components/templates";

export const Home = () => {
  return <HomeTemplate />;
};
